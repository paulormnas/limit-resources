# Limit Resources #

This project is a template to build Docker containers and investigate the time needed to compute an algorithm with differents resources constraints.

### How do I get set up? ###

Use the following commands to set up the project in your machine. Here we need to create a directory named as "reports" in the project directory, where the python script will generate the reports for each container execution. 

```bash
$ git clone https://bitbucket.org/paulormnas/limit-resources.git
$ cd limit-resources
$ mkdir repo
```

Then build and run the scripts with docker-compose command

```bash
$ sudo docker-compose up --build -d
```

To change the resources limits of a container you just need to update the `limits` field in docker-compose.yml file. Also is possible to add new services that should run in parallel with differents constraints.
```yaml
    limits:
      cpus: '0.70'
      memory: 20M
```

In case the container should run in an environment where the cgroup is not activated by default, like in Raspberry Pi, you must add add `cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1` to boot/cmdline.txt

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin