import os
import sys
from datetime import datetime


def fibo(n):
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibo(n - 1) + fibo(n - 2)


def register_results(total_time):
    filename = datetime.now().timestamp()
    with open(f'reports/{filename}', '+a') as f:
        f.write(f'O tempo total foi de: {total_time}')


if __name__ == '__main__':
    print(os.path.abspath("reports"))
    fibo_input = int(sys.argv[1])
    initial_time = datetime.now().timestamp()
    fibo(fibo_input)
    final_time = datetime.now().timestamp()
    total_time = final_time - initial_time
    register_results(total_time)
    print(f'O tempo total foi de: {total_time}')