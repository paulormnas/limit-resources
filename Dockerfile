FROM python:3-alpine

WORKDIR /usr/src/
RUN mkdir -p limit-resources/reports
WORKDIR /usr/src/limit-resources
COPY main.py .

CMD ["python", "main.py", "35"]